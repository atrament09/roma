import React, { useState } from 'react';
import './mustsee.css';
import Button from '@material-ui/core/Button';


export function Mustsee() {
    const places = [
        { value: 'koloseum', name: 'Koloseum', visited: false },
        { value: 'fontanaDiTrevi', name: 'Fontanna di Trevi', visited: false },
        { value: 'piazzaNavona', name: 'Piazza Navona', visited: false },
        { value: 'forumRomanum', name: 'Forum Romanum', visited: false }
    ];
    const [selectValue, setselectValue] = useState("");
    const [mSlist, setMSlist] = useState([])
    const [visit, setVisit] = useState(false)

    const handleonChange = (event) => {
        const { value } = event.target;
        console.log(value);
        setselectValue(value);
    }

    const handleButtonClick = (item) => {
        setMSlist([...mSlist, places.find(place => place.value === selectValue)]);
    }

    const removeClick = (item) => {
        setMSlist([...mSlist.filter((i) => i !== item)]);
    }

    const visitedClick = (value) => {
        setVisit([...mSlist.map(value => value === true)]);
    }

    return (
        <div className="mustseeapp">
            <h2> Wybierz miejsce, które chcesz odwiedzić w Rzymie: </h2>

            <select
                value={selectValue}
                onChange={handleonChange}>
                <option value="">Wybierz z listy</option>
                {places.map(item => !mSlist.includes(item) && <option value={item.value} key={item.value}>{item.name}</option>)}
            </select>

            <Button variant="contained" color="primary" disabled={selectValue === '' || mSlist.includes(selectValue)} onClick={handleButtonClick}> Dodaj do listy</Button>

            <ul>
                {mSlist.map(item =>
                    <li key={item}>
                        {item.name}
                        <Button variant="outlined" color="primary" onClick={() => removeClick(item)}> Usuń </Button>
                        <Button className={visit? "saw" : "" }variant="outlined" color="secondary" onClick={() => visitedClick(item.value)}> Odwiedzone </Button>
                    </li>)}
            </ul>
        </div>
    );
}
