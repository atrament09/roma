import React from 'react';
import './App.css';
import { Slider } from './Slider/Slider';
import { Grid, Container, CssBaseline, Paper } from '@material-ui/core';
import { Mustsee } from './Mustsee/Mustseeapp.js';
import { Quiz } from './Quiz/quiz.js';
import { Miejsca } from './Quiz copy/exercise';

function App() {
  return (
    <>
      <CssBaseline />
      <div className='app-root'>
        <h1 className="title"> ROME </h1>
        <Container maxWidth="md">
          <Grid container direction="row" justify="center" alignItems="center" spacing={10}>
            <Grid item xs={12} spacing={3}>
              <Paper elevation={3}>
                <Slider />
              </Paper>
            </Grid>
          </Grid>
          <Mustsee />
          <Quiz />
          <Miejsca />
        </Container>
      </div>
    </>
  );
}

export default App;
