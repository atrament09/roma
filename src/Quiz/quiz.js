import React, { useState } from 'react';
import './quiz.css';

export function Quiz() {
    const miejsca = [
        { value: 'koloseum', name: 'Koloseum', visited: false },
        { value: 'piazza', name: 'Piazza Navona', visited: false },
        { value: 'spanish', name: 'Spanish Step', visited: false }]

    const [visit, setVisit] = useState(false);

    const odwiedzone = (place) => {
        setVisit([...miejsca.map((i) => place !== true )])
    };

    return (
        <div>
            <ul>
                {miejsca.map(place =>
                    <li className={visit ? "done" : ""} value={place.name} key={place.value}>
                        {place.name}
                        <button onClick={() => odwiedzone(place)}> seen </button> </li>)}
            </ul>
        </div>
    );
}

