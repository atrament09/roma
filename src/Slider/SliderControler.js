import React from "react";
import { Button, ButtonGroup } from "@material-ui/core";

export function SlideControler({ liczbaSlajdow, onClick }) {
  const buttons = Array(liczbaSlajdow).fill()
    .map((item, cyfra) =>
      (
        <Button
          key={cyfra}
          onClick={() => onClick(cyfra)}
          className='btn'>
          {cyfra + 1}
        </Button>
      ))


  return (<>
  <br />
    <ButtonGroup size="large" aria-label="small outlined button group">
      {buttons}
    </ButtonGroup>
    <br />
    <br />
  </>);
}