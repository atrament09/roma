import React from "react";
import { Slide } from "./Slide";
import { SlideControler } from "./SliderControler";
import './Slide.css';


export function Slider() {
    const [currentSlide, setCurrentSlide] = React.useState(0);
    const [slides, setSlides] = React.useState([
        { src: '/assets/Fontana-di-Trevi.jpg', title: 'Fontanna di Trevi', subtitle: 'Piazza di Trevi, 00187 Roma RM, Włochy' },
        { src: '/assets/Koloseum.jpg', title: 'Koloseum', subtitle: 'Piazza del Colosseo, 1, 00184 Roma RM, Włochy' },
        { src: '/assets/Spanish-Steps.jpg', title: 'Schody Hiszpańskie', subtitle: 'Piazza di Spagna, 00187 Roma RM, Włochy' },
        { src: '/assets/Forum-Romanum.jpg', title: 'Forum Romanum', subtitle: 'Via della Salara Vecchia, 5/6, 00186 Roma RM, Włochy' },
    ]);
    const przycisk = (cyfra) => {
        setCurrentSlide(cyfra)
    };

    return (
        <div className="slider-css">
            <SlideControler onClick={przycisk} liczbaSlajdow={slides.length} />
            <Slide slide={slides[currentSlide]} />
            <br/>
        </div>
    );
}