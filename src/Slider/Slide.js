import React from "react";
import { GridListTile, GridListTileBar, GridList } from '@material-ui/core'
import './Slide.css';

export const Slide =
  ({ slide }) => (
    <div className="Slide">
      <GridList>
        <GridListTile>
          <img className="photo" alt={slide.title} src={slide.src} />
          <GridListTileBar
            title={slide.title}
            subtitle={<span>adres zab: {slide.subtitle}</span>}
          />
        </GridListTile>
      </GridList>
    </div>
  );
