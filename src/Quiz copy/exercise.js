import React, { useState } from 'react';
import './exercise.css';

export const Miejsca = () => {
    const [places, setPlaces] = useState([
        { value: 'koloseum', name: 'Koloseum', visited: false },
        { value: 'piazza', name: 'Piazza Navona', visited: false },
        { value: 'spanish', name: 'Spanish Step', visited: false }]);
    return (
        <div>
            <ul>
                {places.map(place => {
                return (<li key={place.value}>{place.name}</li>);
                })}
            </ul>
        </div>
    );  
}
